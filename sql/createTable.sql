CREATE TABLE ADMIN(
  ID INT PRIMARY KEY,
  FNAME VARCHAR(255), 
  SNAME VARCHAR(255),
  MNAME VARCHAR(255),
  LOGIN VARCHAR(255),
  PASSWORD VARCHAR(255)
)




CREATE TABLE insurence_tipe (
    insurence_id INT PRIMARY KEY,
    tipe VARCHAR(10)
)




CREATE TABLE client (
  client_id INT PRIMARY KEY,
  fname VARCHAR(255),
  sname VARCHAR(255),
  mname VARCHAR(255),
  bday date
)




CREATE TABLE insurers (
  insurers_id INT PRIMARY KEY,
  fname VARCHAR(255),
  sname VARCHAR(255),
  mname VARCHAR(255),
  login VARCHAR(255),
  password VARCHAR(255)
)




CREATE TABLE auto (
  auto_id INT PRIMARY KEY,
  fname VARCHAR(255),
  sname VARCHAR(255),
  bday date
)





CREATE TABLE INSURENCE (
  id INT PRIMARY KEY,
  INSURENCE_TYPE_ID INT,
  FOREIGN KEY (INSURENCE_TYPE_ID) REFERENCES INSURENCE_TIPE (INSURENCE_ID),
  INSURERS_ID INT,
  FOREIGN KEY (INSURERS_ID) REFERENCES INSURERS (INSURERS_ID),
  CLIENT_ID INT,
  FOREIGN KEY (CLIENT_ID) REFERENCES CLIENT (CLIENT_ID),
  AUTO_ID INT,
  FOREIGN KEY ( AUTO_ID ) REFERENCES AUTO (AUTO_ID),
  
  begin_date1 date,
  end_date1 date,
 
  people1fsmname VARCHAR(255),
  people1bday date,
  people1prava VARCHAR(255),
  people2fsmname VARCHAR(255),
  people2bday date,
  people2prava VARCHAR(255),
  people3fsmname VARCHAR(255),
  people3bday date,
  people3prava VARCHAR(255),
  people4fsmname VARCHAR(255),
  people4bday date,
  people4prava VARCHAR(255),

  win VARCHAR(25),  
  nomder VARCHAR(25), 
  pts VARCHAR(25)
);



CREATE TABLE price (
  id_price INT PRIMARY KEY,
  INSURENCE_ID INT,
  FOREIGN KEY (INSURENCE_ID) REFERENCES INSURENCE  (ID),
  prise double,
  prisekoef double,
  money double
)